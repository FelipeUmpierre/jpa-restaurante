package com.senars.util;

import java.util.List;

public interface DaoInterface<T> {   
    public void save(T bean);
    public void delete(T bean);
    public T findOne(int id);    
}