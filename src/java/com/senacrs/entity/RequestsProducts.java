/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.senacrs.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Felipe Umpierre
 */
@Entity
@Table(name = "requests_products")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RequestsProducts.findAll", query = "SELECT r FROM RequestsProducts r"),
    @NamedQuery(name = "RequestsProducts.findById", query = "SELECT r FROM RequestsProducts r WHERE r.id = :id"),
    @NamedQuery(name = "RequestsProducts.findByUnityPrice", query = "SELECT r FROM RequestsProducts r WHERE r.unityPrice = :unityPrice"),
    @NamedQuery(name = "RequestsProducts.findByQuantity", query = "SELECT r FROM RequestsProducts r WHERE r.quantity = :quantity"),
    @NamedQuery(name = "RequestsProducts.findByTotalPrice", query = "SELECT r FROM RequestsProducts r WHERE r.totalPrice = :totalPrice"),
    @NamedQuery(name = "RequestsProducts.findByCreatedAt", query = "SELECT r FROM RequestsProducts r WHERE r.createdAt = :createdAt"),
    @NamedQuery(name = "RequestsProducts.findByUpdatedAt", query = "SELECT r FROM RequestsProducts r WHERE r.updatedAt = :updatedAt")})
public class RequestsProducts implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Integer id;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "unity_price")
    private BigDecimal unityPrice;
    @Column(name = "quantity")
    private Integer quantity;
    @Column(name = "total_price")
    private BigDecimal totalPrice;
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @Column(name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "requestsProductsId")
    private List<RequestsObservation> requestsObservationList;
    @JoinColumn(name = "requests_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Requests requestsId;
    @JoinColumn(name = "products_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Products productsId;

    public RequestsProducts() {
    }

    public RequestsProducts(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BigDecimal getUnityPrice() {
        return unityPrice;
    }

    public void setUnityPrice(BigDecimal unityPrice) {
        this.unityPrice = unityPrice;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    @XmlTransient
    public List<RequestsObservation> getRequestsObservationList() {
        return requestsObservationList;
    }

    public void setRequestsObservationList(List<RequestsObservation> requestsObservationList) {
        this.requestsObservationList = requestsObservationList;
    }

    public Requests getRequestsId() {
        return requestsId;
    }

    public void setRequestsId(Requests requestsId) {
        this.requestsId = requestsId;
    }

    public Products getProductsId() {
        return productsId;
    }

    public void setProductsId(Products productsId) {
        this.productsId = productsId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RequestsProducts)) {
            return false;
        }
        RequestsProducts other = (RequestsProducts) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.senacrs.entity.RequestsProducts[ id=" + id + " ]";
    }
    
}
