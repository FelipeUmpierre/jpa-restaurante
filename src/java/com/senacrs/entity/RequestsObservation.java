/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.senacrs.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Felipe Umpierre
 */
@Entity
@Table(name = "requests_observation")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RequestsObservation.findAll", query = "SELECT r FROM RequestsObservation r"),
    @NamedQuery(name = "RequestsObservation.findById", query = "SELECT r FROM RequestsObservation r WHERE r.id = :id"),
    @NamedQuery(name = "RequestsObservation.findByCreatedAt", query = "SELECT r FROM RequestsObservation r WHERE r.createdAt = :createdAt"),
    @NamedQuery(name = "RequestsObservation.findByUpdatedAt", query = "SELECT r FROM RequestsObservation r WHERE r.updatedAt = :updatedAt")})
public class RequestsObservation implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Lob
    @Size(max = 65535)
    @Column(name = "observation")
    private String observation;
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @Column(name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;
    @JoinColumn(name = "requests_products_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private RequestsProducts requestsProductsId;

    public RequestsObservation() {
    }

    public RequestsObservation(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public RequestsProducts getRequestsProductsId() {
        return requestsProductsId;
    }

    public void setRequestsProductsId(RequestsProducts requestsProductsId) {
        this.requestsProductsId = requestsProductsId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RequestsObservation)) {
            return false;
        }
        RequestsObservation other = (RequestsObservation) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.senacrs.entity.RequestsObservation[ id=" + id + " ]";
    }
    
}
