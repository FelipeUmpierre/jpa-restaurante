package com.senacrs.mb;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.senacrs.entity.Categories;
import com.senacrs.facade.CategoriesFacade;
import com.senars.util.Message;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author gremio10
 */
@ManagedBean
@RequestScoped
public class CategoriesMB {
    private Categories instance;
    private final CategoriesFacade facade;
    
    public CategoriesMB() {
        this.instance = new Categories();
        this.facade = new CategoriesFacade();
    }
    
    public String search() {
        return "listarCategorias";
    }
    
    public List<Categories> find() {
        return this.facade.find(this.instance);
    }
    
    public void edit(Categories instance) {
        this.instance = instance;
    }
    
    public String save() {
        try {
            this.facade.save(this.instance);            
            Message.add("Operação executada com sucesso!");
            
            return "listarCategorias";
        } catch(Exception e) {
            Message.error(e.getMessage());
        }
        return null;
    }

    public String delete() {
        try {
            this.facade.delete(this.instance);            
            Message.add("Operação executada com sucesso!");
            
            return "listarCategorias";
        } catch(Exception e) {
            Message.error(e.getMessage());
        }
        return null;
    }

    public Categories getInstance() {
        return instance;
    }
}