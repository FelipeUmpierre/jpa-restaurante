package com.senacrs.mb;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.senacrs.entity.Requests;
import com.senacrs.facade.RequestsFacade;
import com.senars.util.Message;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author gremio10
 */
@ManagedBean
@RequestScoped
public class RequestsMB {
    private Requests instance;
    private final RequestsFacade facade;
    
    public RequestsMB() {
        this.instance = new Requests();
        this.facade = new RequestsFacade();
    }
    
    public String search() {
        return "listarPedidos";
    }
    
    public List<Requests> find() {
        return this.facade.find(this.instance);
    }
    
    public void edit(Requests instance) {
        this.instance = instance;
    }
    
    public String save() {
        try {
            this.facade.save(this.instance);            
            Message.add("Operação executada com sucesso!");
            
            return "listarPedidos";
        } catch(Exception e) {
            Message.error(e.getMessage());
        }
        return null;
    }

    public String delete() {
        try {
            this.facade.delete(this.instance);            
            Message.add("Operação executada com sucesso!");
            
            return "listarPedidos";
        } catch(Exception e) {
            Message.error(e.getMessage());
        }
        return null;
    }

    public Requests getInstance() {
        return instance;
    }
}
