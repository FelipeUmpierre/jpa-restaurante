package com.senacrs.mb;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.senacrs.entity.Boards;
import com.senacrs.facade.BoardsFacade;
import com.senars.util.Message;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author gremio10
 */
@ManagedBean
@RequestScoped
public class BoardsMB {
    private Boards instance;
    private final BoardsFacade facade;
    
    public BoardsMB() {
        this.instance = new Boards();
        this.facade = new BoardsFacade();
    }
    
    public String search() {
        return "listarMesas";
    }
    
    public List<Boards> find() {
        return this.facade.find(this.instance);
    }
    
    public void edit(Boards instance) {
        this.instance = instance;
    }
    
    public String save() {
        try {
            this.facade.save(this.instance);            
            Message.add("Operação executada com sucesso!");
            
            return "listarMesas";
        } catch(Exception e) {
            Message.error(e.getMessage());
        }
        
        return null;
    }

    public String delete() {
        try {
            this.facade.delete(this.instance);            
            Message.add("Operação executada com sucesso!");
            
            return "listarMesas";
        } catch(Exception e) {
            Message.error(e.getMessage());
        }
        
        return null;
    }

    public Boards getInstance() {
        return instance;
    }
}