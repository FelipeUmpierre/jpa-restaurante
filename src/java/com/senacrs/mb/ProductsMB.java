package com.senacrs.mb;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.senacrs.entity.Products;
import com.senacrs.facade.ProductsFacade;
import com.senars.util.Message;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author gremio10
 */
@ManagedBean
@RequestScoped
public class ProductsMB {
    private Products instance;
    private final ProductsFacade facade;
    
    public ProductsMB() {
        this.instance = new Products();
        this.facade = new ProductsFacade();
    }
    
    public String search() {
        return "listarProdutos";
    }
    
    public List<Products> find() {
        return this.facade.find(this.instance.getName());
    }
    
    public void edit(Products instance) {
        this.instance = instance;
    }
    
    public String save() {
        try {
            this.facade.save(this.instance);            
            Message.add("Operação executada com sucesso!");
            
            return "listarProdutos";
        } catch(Exception e) {
            Message.error(e.getMessage());
        }
        return null;
    }

    public String delete() {
        try {
            this.facade.delete(this.instance);            
            Message.add("Operação executada com sucesso!");
            
            return "listarProdutos";
        } catch(Exception e) {
            Message.error(e.getMessage());
        }
        return null;
    }

    public Products getInstance() {
        return instance;
    }
}
