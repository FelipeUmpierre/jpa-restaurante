/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.senacrs.mb;

import com.senacrs.dao.UsersDao;
import com.senacrs.entity.Users;
import com.senacrs.facade.UsersFacade;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.persistence.NoResultException;

/**
 *
 * @author gremio10
 */
@ManagedBean
@RequestScoped
public class AuthMB 
{
    private Users instance;
    private final UsersFacade facade;
    
    /**
     * Creates a new instance of AuthMB
     */
    public AuthMB() 
    {
        this.instance = new Users();
        this.facade = new UsersFacade();
    }

    public String auth()
    {
        try {
            Users u = this.facade.auth(this.instance);
            
            return "/view/index";
        } catch(NoResultException e) {
            return "/view/login/index";
        }
    }
    
    public Users getInstance() {
        return instance;
    }
}
