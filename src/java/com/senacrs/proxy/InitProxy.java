package com.senacrs.proxy;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "InitProxy", urlPatterns = {"/InitProxy"}, loadOnStartup = 2)
public class InitProxy extends HttpServlet {

    @Override
    public void init() throws ServletException {
        super.init();
//        System.setProperty("http.proxyHost", "192.168.7.253");
//        System.setProperty("http.proxyPort", "80");
        System.out.println(">>>> estabelecendo conexão com proxy");
    }    

}
