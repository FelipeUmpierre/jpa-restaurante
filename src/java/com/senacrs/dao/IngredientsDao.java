/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.senacrs.dao;

import com.senacrs.entity.Ingredients;
import com.senars.util.DaoInterface;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Felipe Umpierre
 */
public class IngredientsDao implements DaoInterface<Ingredients> {

    private EntityManager em;
    
    public IngredientsDao() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("checkfood");
        this.em = emf.createEntityManager();
    }
    
    @Override
    public void save(Ingredients bean) {
        this.em.getTransaction().begin();
        
        try {
            em.merge(bean);
            
            this.em.getTransaction().commit();
        } catch(Exception e) {
            this.em.getTransaction().rollback();
        }
    }

    @Override
    public void delete(Ingredients bean) {
        this.em.getTransaction().begin();
        
        try {
            em.remove(em.find(Ingredients.class, bean.getId()));
            
            this.em.getTransaction().commit();
        } catch(Exception e) {
            this.em.getTransaction().rollback();
        }
    }

    public List<Ingredients> find(Ingredients bean) {
        return (List<Ingredients>) em.createNamedQuery("Ingredients.findAll").getResultList();
    }

    @Override
    public Ingredients findOne(int id) {
        return (Ingredients) em.createNamedQuery("Ingredients.findById").setParameter("id", id).getSingleResult();
    }
}