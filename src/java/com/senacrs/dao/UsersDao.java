/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.senacrs.dao;

import com.senacrs.entity.Users;
import com.senars.util.DaoInterface;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author Felipe Umpierre
 */
public class UsersDao implements DaoInterface<Users> {

    private EntityManager em;
    
    public UsersDao() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("checkfood");
        this.em = emf.createEntityManager();
    }
    
    @Override
    public void save(Users bean) {
        this.em.getTransaction().begin();
        
        try {
            em.merge(bean);
            
            this.em.getTransaction().commit();
        } catch(Exception e) {
            this.em.getTransaction().rollback();
        }
    }

    @Override
    public void delete(Users bean) {
        this.em.getTransaction().begin();
        
        try {
            em.remove(em.find(Users.class, bean.getId()));
            
            this.em.getTransaction().commit();
        } catch(Exception e) {
            this.em.getTransaction().rollback();
        }
    }

    public List<Users> find(Users bean) {
        return (List<Users>) em.createNamedQuery("Users.findAll").getResultList();
    }

    @Override
    public Users findOne(int id) {
        return (Users) em.createNamedQuery("Users.findById").setParameter("id", id).getSingleResult();
    }
    
    public Users auth(Users bean) {
        try {
            return (Users) em.createQuery("SELECT u FROM Users u WHERE u.login LIKE :login AND u.password LIKE :password").setParameter("login", bean.getLogin()).setParameter("password", bean.getPassword()).getSingleResult();
        } catch(NoResultException e) {
            return null;
        }
    }
}