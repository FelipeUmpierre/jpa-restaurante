/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.senacrs.dao;

import com.senacrs.entity.Categories;
import com.senars.util.DaoInterface;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author Felipe Umpierre
 */
public class CategoriesDao implements DaoInterface<Categories> {

    private EntityManager em;
    
    public CategoriesDao() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("checkfood");
        this.em = emf.createEntityManager();
    }
    
    @Override
    public void save(Categories bean) {
        this.em.getTransaction().begin();
        
        try {
            em.merge(bean);
            
            this.em.getTransaction().commit();
        } catch(Exception e) {
            this.em.getTransaction().rollback();
        }
    }

    @Override
    public void delete(Categories bean) {
        this.em.getTransaction().begin();
        
        try {
            em.remove(em.find(Categories.class, bean.getId()));
            
            this.em.getTransaction().commit();
        } catch(Exception e) {
            this.em.getTransaction().rollback();
        }
    }

    public List<Categories> find(Categories bean) {
        StringBuilder sb = new StringBuilder("SELECT c FROM Categories c WHERE 1 = 1 ");

        if(bean.getName() != null && !"".equals(bean.getName())) {
            sb.append("AND c.name LIKE :n ");
        }
        
        sb.append("ORDER BY c.name");
        
        Query qry = em.createQuery(sb.toString());
        
        if(bean.getName() != null && !"".equals(bean.getName())) {
            qry.setParameter("n", "%" + bean.getName() + "%");
        }
        
        return qry.getResultList();
    }

    @Override
    public Categories findOne(int id) {
        return (Categories) em.createNamedQuery("Categories.findById").setParameter("id", id).getSingleResult();
    }
}