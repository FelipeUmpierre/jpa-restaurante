/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.senacrs.dao;

import com.senacrs.entity.Requests;
import com.senars.util.DaoInterface;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Felipe Umpierre
 */
public class RequestsDao implements DaoInterface<Requests> {

    private EntityManager em;
    
    public RequestsDao() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("checkfood");
        this.em = emf.createEntityManager();
    }
    
    @Override
    public void save(Requests bean) {
        this.em.getTransaction().begin();
        
        try {
            em.merge(bean);
            
            this.em.getTransaction().commit();
        } catch(Exception e) {
            this.em.getTransaction().rollback();
        }
    }

    @Override
    public void delete(Requests bean) {
        this.em.getTransaction().begin();
        
        try {
            em.remove(em.find(Requests.class, bean.getId()));
            
            this.em.getTransaction().commit();
        } catch(Exception e) {
            this.em.getTransaction().rollback();
        }
    }

    public List<Requests> find(Requests bean) {
        return (List<Requests>) em.createNamedQuery("Requests.findAll").getResultList();
    }

    @Override
    public Requests findOne(int id) {
        return (Requests) em.createNamedQuery("Requests.findById").setParameter("id", id).getSingleResult();
    }
}