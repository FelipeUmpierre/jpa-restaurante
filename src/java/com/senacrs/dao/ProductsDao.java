/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.senacrs.dao;

import com.senacrs.entity.Products;
import com.senars.util.DaoInterface;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author Felipe Umpierre
 */
public class ProductsDao implements DaoInterface<Products> {

    private EntityManager em;
    
    public ProductsDao() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("checkfood");
        this.em = emf.createEntityManager();
    }
    
    @Override
    public void save(Products bean) {
        this.em.getTransaction().begin();
        
        try {
            em.merge(bean);
            
            this.em.getTransaction().commit();
        } catch(Exception e) {
            this.em.getTransaction().rollback();
        }
    }

    @Override
    public void delete(Products bean) {
        this.em.getTransaction().begin();
        
        try {
            em.remove(em.find(Products.class, bean.getId()));
            
            this.em.getTransaction().commit();
        } catch(Exception e) {
            this.em.getTransaction().rollback();
        }
    }

    public List<Products> find(String name) {
        StringBuilder sb = new StringBuilder("SELECT p FROM Products p WHERE 1 = 1 ");

        if(name != null && !"".equals(name)) {
            sb.append("AND p.name LIKE :n ");
        }
        
        sb.append("ORDER BY p.name");
        
        Query qry = em.createQuery(sb.toString());
        
        if(name != null && !"".equals(name)) {
            qry.setParameter("n", "%" + name + "%");
        }
        
        return qry.getResultList();
    }

    @Override
    public Products findOne(int id) {
        return (Products) em.createNamedQuery("Products.findById").setParameter("id", id).getSingleResult();
    }
    
    public List<Products> findByPrice(double price) {
        StringBuilder sb = new StringBuilder("SELECT p FROM Products p WHERE 1 = 1 ");
        sb.append("AND p.price LIKE :n ");
        sb.append("ORDER BY p.name");
        
        Query qry = em.createQuery(sb.toString());       
        qry.setParameter("n", price);
        
        return qry.getResultList();
    }
    
    public List<Products> findByStock(int stock) {
        StringBuilder sb = new StringBuilder("SELECT p FROM Products p WHERE 1 = 1 ");
        sb.append("AND p.stock LIKE :n ");
        sb.append("ORDER BY p.name");
        
        Query qry = em.createQuery(sb.toString());       
        qry.setParameter("n", stock);
        
        return qry.getResultList();
    }
    
    public List<Products> findByCategoriesId(int categoriesId) {
        StringBuilder sb = new StringBuilder("SELECT p FROM Products p WHERE 1 = 1 ");
        sb.append("AND p.price LIKE :n ");
        sb.append("ORDER BY p.categories_id");
        
        Query qry = em.createQuery(sb.toString());       
        qry.setParameter("n", categoriesId);
        
        return qry.getResultList();
    }
}