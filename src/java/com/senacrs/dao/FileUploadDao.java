/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.senacrs.dao;

import com.senacrs.entity.FileUpload;
import com.senars.util.DaoInterface;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Felipe Umpierre
 */
public class FileUploadDao implements DaoInterface<FileUpload> {

    private EntityManager em;
    
    public FileUploadDao() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("checkfood");
        this.em = emf.createEntityManager();
    }
    
    @Override
    public void save(FileUpload bean) {
        this.em.getTransaction().begin();
        
        try {
            em.merge(bean);
            
            this.em.getTransaction().commit();
        } catch(Exception e) {
            this.em.getTransaction().rollback();
        }
    }

    @Override
    public void delete(FileUpload bean) {
        this.em.getTransaction().begin();
        
        try {
            em.remove(em.find(FileUpload.class, bean.getId()));
            
            this.em.getTransaction().commit();
        } catch(Exception e) {
            this.em.getTransaction().rollback();
        }
    }

    public List<FileUpload> find(FileUpload bean) {
        return (List<FileUpload>) em.createNamedQuery("FileUpload.findAll").getResultList();
    }

    @Override
    public FileUpload findOne(int id) {
        return (FileUpload) em.createNamedQuery("FileUpload.findById").setParameter("id", id).getSingleResult();
    }
}