/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.senacrs.dao;

import com.senacrs.entity.Boards;
import com.senars.util.DaoInterface;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author Felipe Umpierre
 */
public class BoardsDao implements DaoInterface<Boards> {

    private EntityManager em;

    public BoardsDao() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("checkfood");
        this.em = emf.createEntityManager();
    }

    @Override
    public void save(Boards bean) {
        this.em.getTransaction().begin();

        try {
            em.merge(bean);
            em.flush();

            this.em.getTransaction().commit();
        } catch (Exception e) {
            this.em.getTransaction().rollback();
        }
    }

    @Override
    public void delete(Boards bean) {
        this.em.getTransaction().begin();

        try {
            em.remove(em.find(Boards.class, bean.getId()));

            this.em.getTransaction().commit();
        } catch (Exception e) {
            this.em.getTransaction().rollback();
        }
    }

    public List<Boards> find(Boards bean) {
        StringBuilder sb = new StringBuilder("SELECT b FROM Boards b WHERE 1 = 1 ");

        if(bean.getNumber() != null && !"".equals(bean.getNumber())) {
            sb.append("AND b.number LIKE :n ");
        }
        
        sb.append("ORDER BY b.number");
        
        Query qry = em.createQuery(sb.toString());
        
        if(bean.getNumber() != null && !"".equals(bean.getNumber())) {
            qry.setParameter("n", "%" + bean.getNumber() + "%");
        }
        
        return qry.getResultList();
    }

    @Override
    public Boards findOne(int id) {
        return (Boards) em.createNamedQuery("Boards.findById").setParameter("id", id).getSingleResult();
    }
}
