/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.senacrs.dao;

import com.senacrs.entity.Status;
import com.senars.util.DaoInterface;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Felipe Umpierre
 */
public class StatusDao implements DaoInterface<Status> {

    private EntityManager em;
    
    public StatusDao() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("checkfood");
        this.em = emf.createEntityManager();
    }
    
    @Override
    public void save(Status bean) {
        this.em.getTransaction().begin();
        
        try {
            em.merge(bean);
            
            this.em.getTransaction().commit();
        } catch(Exception e) {
            this.em.getTransaction().rollback();
        }
    }

    @Override
    public void delete(Status bean) {
        this.em.getTransaction().begin();
        
        try {
            em.remove(em.find(Status.class, bean.getId()));
            
            this.em.getTransaction().commit();
        } catch(Exception e) {
            this.em.getTransaction().rollback();
        }
    }

    public List<Status> find(Status Bean) {
        return (List<Status>) em.createNamedQuery("Status.findAll").getResultList();
    }

    @Override
    public Status findOne(int id) {
        return (Status) em.createNamedQuery("Status.findById").setParameter("id", id).getSingleResult();
    }
}