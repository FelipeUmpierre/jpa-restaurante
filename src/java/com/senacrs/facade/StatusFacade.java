/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.senacrs.facade;

import com.senacrs.dao.StatusDao;
import com.senacrs.entity.Status;
import java.util.List;

/**
 *
 * @author Felipe Umpierre
 */
public class StatusFacade {
    private final StatusDao dao;
    
    public StatusFacade() {
        this.dao = new StatusDao();
    }
    
    public void save(Status instance) {
        this.dao.save(instance);
    }
    
    public void delete(Status instance) {
        this.dao.delete(instance);
    }
    
    public List<Status> find(Status instance) {
        return this.dao.find(instance);
    }
    
    public Status findOne(int id) {
        return this.dao.findOne(id);
    }
}
