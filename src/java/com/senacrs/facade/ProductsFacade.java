/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.senacrs.facade;

import com.senacrs.dao.ProductsDao;
import com.senacrs.entity.Products;
import java.util.List;
import javax.jws.WebService;

/**
 *
 * @author Felipe Umpierre
 */
@WebService
public class ProductsFacade {
    private final ProductsDao dao;
    
    public ProductsFacade() {
        this.dao = new ProductsDao();
    }
    
    public void save(Products instance) {
        this.dao.save(instance);
    }
    
    public void delete(Products instance) {
        this.dao.delete(instance);
    }
    
    public List<Products> find(String instance) {
        return this.dao.find(instance);
    }
    
    public Products findOne(int id) {
        return this.dao.findOne(id);
    }
    
    public List<Products> findByPrice(double price) {
        return this.dao.findByPrice(price);
    }
    
    public List<Products> findByStock(int stock) {
        return this.dao.findByStock(stock);
    }
    
    public List<Products> findByCategoriesId(int categoriesId) {
        return this.dao.findByCategoriesId(categoriesId);
    }
}