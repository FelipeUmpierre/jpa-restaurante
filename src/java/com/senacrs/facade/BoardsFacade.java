/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.senacrs.facade;

import com.senacrs.dao.BoardsDao;
import com.senacrs.entity.Boards;
import java.util.List;

/**
 *
 * @author Felipe Umpierre
 */
public class BoardsFacade {
    private final BoardsDao dao;
    
    public BoardsFacade() {
        this.dao = new BoardsDao();
    }
    
    public void save(Boards instance) {
        this.dao.save(instance);
    }
    
    public void delete(Boards instance) {
        this.dao.delete(instance);
    }
    
    public List<Boards> find(Boards instance) {
        return this.dao.find(instance);
    }
    
    public Boards findOne(int id) {
        return this.dao.findOne(id);
    }
}
