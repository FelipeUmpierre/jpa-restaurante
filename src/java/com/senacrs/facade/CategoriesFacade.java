/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.senacrs.facade;

import com.senacrs.dao.CategoriesDao;
import com.senacrs.entity.Categories;
import java.util.List;

/**
 *
 * @author Felipe Umpierre
 */
public class CategoriesFacade {
    private final CategoriesDao dao;
    
    public CategoriesFacade() {
        this.dao = new CategoriesDao();
    }
    
    public void save(Categories instance) {
        this.dao.save(instance);
    }
    
    public void delete(Categories instance) {
        this.dao.delete(instance);
    }
    
    public List<Categories> find(Categories instance) {
        return this.dao.find(instance);
    }
    
    public Categories findOne(int id) {
        return this.dao.findOne(id);
    }
}
