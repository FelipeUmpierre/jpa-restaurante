/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.senacrs.facade;

import com.senacrs.dao.FileUploadDao;
import com.senacrs.entity.FileUpload;
import java.util.List;

/**
 *
 * @author Felipe Umpierre
 */
public class FileUploadFacade {
    private final FileUploadDao dao;
    
    public FileUploadFacade() {
        this.dao = new FileUploadDao();
    }
    
    public void save(FileUpload instance) {
        this.dao.save(instance);
    }
    
    public void delete(FileUpload instance) {
        this.dao.delete(instance);
    }
    
    public List<FileUpload> find(FileUpload instance) {
        return this.dao.find(instance);
    }
    
    public FileUpload findOne(int id) {
        return this.dao.findOne(id);
    }
}
