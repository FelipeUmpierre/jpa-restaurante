/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.senacrs.facade;

import com.senacrs.dao.UsersDao;
import com.senacrs.entity.Users;
import java.util.List;

/**
 *
 * @author Felipe Umpierre
 */
public class UsersFacade {
    private final UsersDao dao;
    
    public UsersFacade() {
        this.dao = new UsersDao();
    }
    
    public void save(Users instance) {
        this.dao.save(instance);
    }
    
    public void delete(Users instance) {
        this.dao.delete(instance);
    }
    
    public List<Users> find(Users instance) {
        return this.dao.find(instance);
    }
    
    public Users findOne(int id) {
        return this.dao.findOne(id);
    }
    
    public Users auth(Users instance) {
        return this.dao.auth(instance);
    }
}
