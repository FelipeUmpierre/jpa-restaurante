/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.senacrs.facade;

import com.senacrs.dao.RequestsDao;
import com.senacrs.entity.Requests;
import java.util.List;

/**
 *
 * @author Felipe Umpierre
 */
public class RequestsFacade {
    private final RequestsDao dao;
    
    public RequestsFacade() {
        this.dao = new RequestsDao();
    }
    
    public void save(Requests instance) {
        this.dao.save(instance);
    }
    
    public void delete(Requests instance) {
        this.dao.delete(instance);
    }
    
    public List<Requests> find(Requests instance) {
        return this.dao.find(instance);
    }
    
    public Requests findOne(int id) {
        return this.dao.findOne(id);
    }
}
