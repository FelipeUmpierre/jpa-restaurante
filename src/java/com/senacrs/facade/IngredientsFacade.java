/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.senacrs.facade;

import com.senacrs.dao.IngredientsDao;
import com.senacrs.entity.Ingredients;
import java.util.List;

/**
 *
 * @author Felipe Umpierre
 */
public class IngredientsFacade {
    private final IngredientsDao dao;
    
    public IngredientsFacade() {
        this.dao = new IngredientsDao();
    }
    
    public void save(Ingredients instance) {
        this.dao.save(instance);
    }
    
    public void delete(Ingredients instance) {
        this.dao.delete(instance);
    }
    
    public List<Ingredients> find(Ingredients instance) {
        return this.dao.find(instance);
    }
    
    public Ingredients findOne(int id) {
        return this.dao.findOne(id);
    }
}
